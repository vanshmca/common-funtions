import { EventRegister } from "react-native-event-listeners"

export default SnackBarManager = {
    show: function (message) {
        EventRegister.emit('snackBar', message)
    }
}