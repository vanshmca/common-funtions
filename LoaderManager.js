import { EventRegister } from 'react-native-event-listeners';

export default LoaderManager = {
    show: function () {
        EventRegister.emit('loader', true)
    },
    hide: function () {
        EventRegister.emit('loader', false)
    }
}