import { Alert } from 'react-native'
import { EventRegister } from 'react-native-event-listeners'
import firebase from '@react-native-firebase/app';
import SimpleToast from 'react-native-simple-toast';
import Const from './Const';
import LoaderManager from './LoaderManager'
import { navigate, reset } from './NavigationService';
import AsyncStorage from '@react-native-community/async-storage';

export default Api = {

    call: async function (url, method = 'GET', bodyData = null, hastoken = true, loaderNeeded = true) {
        var header = {};
        var fullUrl = Const.baseUrl + url;
        if (bodyData instanceof FormData) {
            if (hastoken) {
                header = {
                    "Authorization": global.token
                }
            }
            else {
                header = {
                    "Accept": "application/json",
                }
            }
        } else {
            if (hastoken) {
                header = {
                    "Content-Type": "application/json",
                    "Authorization": global.token
                }
            }
            else {
                header = {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }
            }
        }
        // console.log('header-->', header, method)
        console.log('url-->', fullUrl)
        console.log('body-->', bodyData)

        if (loaderNeeded) {
            LoaderManager.show()
        }

        return fetch(fullUrl, {
            method: method,
            body: bodyData,
            headers: header,
        }).then((response) => {
            // console.log(response)

            if (loaderNeeded) {
                LoaderManager.hide()
            }

            if (response.status == 401) {
                // firebase.notifications().removeAllDeliveredNotifications()
                AsyncStorage.getItem('isLoggedIn').then(data => {
                    console.log(data)
                    if (data == "true") {
                        AsyncStorage.clear()
                        reset('SocialLogin')
                    }
                })
            }
            else if (response.status == 201 || response.status == 200) {
                return response.json()
            } else {
                response.json().then((responseResult) => {
                    console.log(responseResult)
                    setTimeout(() => {
                        EventRegister.emit('snackBar', responseResult.message)
                    }, 500);
                })
            }
        })
            .then((responseJson) => {
                return responseJson
            })
            .catch((error) => {
                EventRegister.emit('loader', false)
                console.log(error)
                setTimeout(() => {
                    SimpleToast.show("Please check your internet and try again.")
                    // Alert.alert(
                    //     'Connection Failed',
                    //     'Please check your internet and try again.',
                    //     [
                    //         {
                    //             text: 'Ok',
                    //             onPress: () => { console.log('cancelled') }
                    //         },
                    //         // {
                    //         //     text: 'Retry',
                    //         //     onPress: () => { }
                    //         // },
                    //     ]
                    // )
                }, 500);

                console.log('catch Eroor-->' + error);
            });
    },
}