import React, { useState, useRef, useEffect } from 'react';
import ReactNativeModal from "react-native-modal"
import { View, Text, Image, TouchableOpacity, TextInput, ScrollView, FlatList, Platform, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Dimensions, SafeAreaView } from "react-native"
import styles, { fonts } from "./styles"
import colors from './colors';
import Button from '../components/Button';
import { color } from 'react-native-reanimated';
import StarRating from 'react-native-star-rating';
import moment from "moment";
import { EventRegister } from 'react-native-event-listeners';
import Toast from 'react-native-simple-toast'
import { symbol } from 'prop-types';
import Api from './Api';
import SimpleToast from 'react-native-simple-toast';
import { CalendarList } from 'react-native-calendars';
import Divider from '../components/Divider';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Clipboard from "@react-native-community/clipboard";

export function adHoc(isVisible, onBackButtonPress) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            style={{ margin: 40 }}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <View style={[styles.iconJobType, styles.darkShadow, { borderColor: colors.appBlue }]}>
                    <Image source={require('../assets/icons/add-hoc.png')} style={[styles.iconLarge]} />
                </View>
                <Text style={styles.textInfoModalTitle}>What is Ad hoc?</Text>
                <View style={{ marginTop: 20, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>
                        Ad hoc jobs refer to those that require you to work between 1 to 2 days and you are not bounded by a
                        contract to work for a period of time, e.g. 3 months {'\n\n'}
                        An example would be giving out flyers on the 9th of June, or being a banquet server on the 30th of May.
                    </Text>
                </View>
            </View>
        </ReactNativeModal>
    )
}

export function partTime(isVisible, onBackButtonPress) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            style={{ margin: 40 }}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <View style={[styles.iconJobType, styles.darkShadow, { borderColor: colors.appBlue }]}>
                    <Image source={require('../assets/icons/parttime.png')} style={[styles.iconLarge]} />
                </View>
                <Text style={styles.textInfoModalTitle}>What is Part Time?</Text>
                <View style={{ marginTop: 20, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>
                        Part time jobs refer to those where you are under a contract of service to work less than 35 hours per week.
                        {'\n\n'}An example would be a contract to work at Starbucks for 6 months, but only during the weekends.
                    </Text>
                </View>
            </View>
        </ReactNativeModal>
    )
}

export function fullTime(isVisible, onBackButtonPress) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            style={{ margin: 40 }}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <View style={[styles.iconJobType, styles.darkShadow, { borderColor: colors.appBlue }]}>
                    <Image source={require('../assets/icons/full-time.png')} style={[styles.iconLarge]} />
                </View>
                <Text style={styles.textInfoModalTitle}>What is Full Time?</Text>
                <View style={{ marginTop: 20, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>
                        Full time jobs refer to those where you are under a contract of service to work more than 35 hours per week.
                    {'\n\n'}
                    An example would be a contract to work at a law firm from Monday to Friday, 9.00am to 5.00pm daily.
                    </Text>
                </View>
            </View>
        </ReactNativeModal>
    )
}

export function locationInfoModal(isVisible, onBackButtonPress) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>What is this for?</Text>
                <View style={{ marginTop: 10, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Your postal code will help us to{'\n'} recommend jobs near you.</Text>
                </View>
            </View>
        </ReactNativeModal>
    )
}


export function emailSentModal(isVisible, email: String, onBackButtonPress) {
    let len = 0
    if (email.split('@')[0].length > 4) {
        len = email.split('@')[0].length - 4
    }

    email = email.substr(len, email.length)

    for (let i = 0; i < len; i++) {
        email = "*" + email
    }

    console.log(email.split('@')[0].length);
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Image source={require('../assets/icons/check-rounded.png')} style={[styles.icon80, { marginTop: 20 }]} />
                <Text style={[styles.textInfoModalContent, { textAlign: 'center', marginTop: 10 }]}>An email has just been sent to the{'\n'} address you entered:</Text>
                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center', color: colors.appGold }]}>{email}</Text>
                </View>
            </View>
        </ReactNativeModal>
    )
}

export function jobsBasedOnSearch(isVisible, onBackButtonPress, onEditPreferences) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Jobs based on your interests</Text>
                <View style={{ marginTop: 10, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center', fontFamily: fonts.regular }]}>
                        Jobs we recommend are based on the{'\n'}preferences you have indicated when{'\n'}you created your profile.
                        {'\n'}
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontFamily: fonts.regular }}>Edit your preferences </Text>
                        <Text onPress={onEditPreferences} style={{ color: colors.appBlue, fontFamily: fonts.semiBold, textDecorationLine: 'underline' }}>here!</Text>
                    </View>
                </View>
            </View>
        </ReactNativeModal>
    )
}

export function PlatformIsNew(isVisible, onBackButtonPress) {
    const [isPositive, setIsPositive] = useState(false);

    const [isAdHoc, setAdHoc] = useState(false);
    const [isPartTime, setPartTime] = useState(false);
    const [isFullTime, setFullTime] = useState(false);

    const [adHocModal, setAdHocModal] = useState(false);
    const [partTimeModal, setPartTimeModal] = useState(false);
    const [fullTimeModal, setFullTimeModal] = useState(false);

    const toggleAdhoc = () => {
        setAdHocModal(!adHocModal)
    }

    const togglePartTime = () => {
        setPartTimeModal(!partTimeModal)
    }

    const toggleFullTime = () => {
        setFullTimeModal(!fullTimeModal)
    }

    function setInstructions() {
        if (isPositive) {
            let jobType = []
            if (isAdHoc) {
                jobType.push("1")
            } if (isPartTime) {
                jobType.push("2")
            } if (isFullTime) {
                jobType.push("3")
            }
            if (jobType.length == 0) {
                SimpleToast.show('Please select at least one type')
            } else {
                let body = {
                    "employer_notification": "True",
                    "job_type": jobType
                }
                Api.call('new_company_notification', 'POST', JSON.stringify(body)).then(res => {
                    console.log(res)
                    if (res.status == 1) {
                        onBackButtonPress()
                    }
                })
            }
        } else {
            onBackButtonPress()
        }
    }

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            style={{ margin: 0 }}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Image source={require('../assets/icons/image-job-popup.png')} style={[styles.icon80]} />
                <View style={{ marginTop: 20, alignItems: 'center', width: '100%' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>
                        The platform is new to the market so we have limited employers.
                        {'\n\n'}
                        Do you want to be notified when a new employer joins?
                    </Text>
                    <View style={{ flexDirection: 'row', marginVertical: 20, marginHorizontal: 4, alignItems: 'center' }}>

                        <TouchableOpacity style={{ flexDirection: 'row' }}
                            activeOpacity={1} onPress={() => setIsPositive(true)}>
                            < Image style={{ height: 20, width: 20, marginEnd: 8 }} source={
                                isPositive ?
                                    require('../assets/icons/radio-check.png') :
                                    require('../assets/icons/radio.png')
                            } />
                            <Text style={{ fontFamily: fonts.medium, color: colors.appBlue, bottom: 1 }}>Yes</Text>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={1} style={{ marginStart: 10, flexDirection: 'row' }}
                            onPress={() => setIsPositive(false)}>
                            <Image style={{ height: 20, width: 20, marginEnd: 8 }} source={
                                !isPositive ?
                                    require('../assets/icons/radio-check.png') :
                                    require('../assets/icons/radio.png')
                            } />
                            <Text style={{ fontFamily: fonts.medium, color: colors.appBlue, bottom: 1 }}>No</Text>
                        </TouchableOpacity>
                    </View>


                    {isPositive ?
                        <View style={{ width: '100%', alignItems: 'center' }}>
                            <Text style={{ color: 'gray', fontFamily: fonts.medium, marginBottom: 10 }}>For these employers:</Text>

                            <View style={{ flexDirection: 'row', marginVertical: 16 }}>

                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => setAdHoc(!isAdHoc)}
                                        style={[styles.iconJobTypeModal, styles.darkShadow,
                                        { borderColor: isAdHoc ? colors.appBlue : '#DBDBDB' }]}>
                                        <Image source={require('../assets/icons/add-hoc.png')} style={[styles.icon32]} />
                                    </TouchableOpacity>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
                                        <Text style={{ fontFamily: fonts.medium, marginEnd: 4 }}>Ad hoc</Text>
                                        <TouchableOpacity onPress={toggleAdhoc}>
                                            <Image source={require('../assets/icons/help.png')} style={[styles.iconMicro, { top: 2 }]} />
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => setPartTime(!isPartTime)}
                                        style={[styles.iconJobTypeModal, styles.darkShadow,
                                        { borderColor: isPartTime ? colors.appBlue : '#DBDBDB' }]}>
                                        <Image source={require('../assets/icons/parttime.png')} style={[styles.icon32]} />
                                    </TouchableOpacity>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
                                        <Text style={{ fontFamily: fonts.medium, marginEnd: 4 }}>Part Time</Text>
                                        <TouchableOpacity onPress={togglePartTime}>
                                            <Image source={require('../assets/icons/help.png')} style={[styles.iconMicro, { top: 2 }]} />
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => setFullTime(!isFullTime)} style={[styles.iconJobTypeModal, styles.darkShadow,
                                    { borderColor: isFullTime ? colors.appBlue : '#DBDBDB' }]}>
                                        <Image source={require('../assets/icons/full-time.png')} style={[styles.icon32]} />
                                    </TouchableOpacity>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
                                        <Text style={{ fontFamily: fonts.medium, marginEnd: 4 }}>Full Time</Text>
                                        <TouchableOpacity onPress={toggleFullTime}>
                                            <Image source={require('../assets/icons/help.png')} style={[styles.iconMicro, { top: 2 }]} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                        : null}

                    <Button
                        title={"Okay"}
                        style={{ marginTop: 10, width: '100%' }}
                        backgroundColor={colors.appGold}
                        textColor={'white'}
                        onPress={setInstructions} />
                </View>
                {adHoc(adHocModal, toggleAdhoc)}
                {partTime(partTimeModal, togglePartTime)}
                {fullTime(fullTimeModal, toggleFullTime)}
            </View>
        </ReactNativeModal>
    )
}

export function cancelled(isVisible, company, post, text2, onBackButtonPress, onApplyForAnother) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <View style={{ marginTop: 20, alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
                        <Text style={[{ marginEnd: 4 }, styles.notiText]}>Your job at
                            <Text style={styles.notiCompName}>{" "}{company}{" "}</Text>
                            <Text style={[{ marginStart: 4 }, styles.notiText]}>as a</Text>
                            <Text style={styles.notiPost}>{" "}{post}{" "}</Text>
                            <Text style={[{ marginStart: 4 }, styles.notiText]}>has been cancelled.</Text>
                        </Text>
                    </View>
                    {text2 && text2.length > 0 && <Text style={{ fontSize: 12, fontFamily: fonts.medium, color: 'gray', marginTop: 10 }}>
                        Reason given by employer
                    </Text>}
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center', fontSize: 13 }]}>
                        {text2}
                    </Text>
                </View>

                <Button
                    title={"Apply For Another Job"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={onApplyForAnother} />
            </View>
        </ReactNativeModal>
    )
}


export function pointsEarned(isVisible, name, onBackButtonPress, onCheckPressed) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Image source={require('../assets/icons/cong.png')} style={[styles.icon80, { marginTop: 10 }]} />

                <Text style={[styles.textInfoModalTitle, { color: '#21BD9D', marginTop: 10 }]}>Congratulations!</Text>
                <Text style={[styles.textInfoModalContent, { textAlign: 'center', color: '#909090', marginTop: 20 }]}>Points Earned</Text>
                <Text style={[{ color: '#21BD9D', fontSize: 30, fontFamily: fonts.bold }]}>50</Text>

                <View style={{ alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center', lineHeight: 20 }]}>Your friend <Text style={{ fontFamily: fonts.bold }}>{name}</Text> has{'\n'} successfully signed up!</Text>
                </View>

                <Button
                    title={"Check Rewards"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={onCheckPressed} />

            </View>
        </ReactNativeModal>
    )
}

export function blockUser(isVisible, onBackButtonPress, onYesPressed) {

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Block this user?</Text>
                <View style={{ marginTop: 20, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Are you sure you want to block{'\n'}this user?</Text>
                </View>

                <Button
                    title={"Yes"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={onYesPressed} />

                <Button
                    title={"No"}
                    style={{ marginTop: 10, width: '100%' }}
                    backgroundColor={colors.appBlue}
                    textColor={'white'}
                    onPress={onBackButtonPress} />

            </View>
        </ReactNativeModal>
    )
}


export function tellUsWhy(isVisible, id, onBackButtonPress, onSubmit) {
    const [index, setIndex] = useState(0);
    const [other, setOther] = useState('');

    function isValid() {
        if (index == 3 && other.length == 0) {
            Toast.show("Other reason field cannot be empty.")
        } else {
            let reason
            if (index == 0) {
                reason = "Feels like spam"
            } else if (index == 1) {
                reason = "Rude"
            } else if (index == 2) {
                reason = "Inappropriate"
            } else if (index == 3) {
                reason = other
            }
            let body = {
                "user_block": id,
                "reason": reason
            }
            Api.call('block_user', 'POST', JSON.stringify(body)).then(res => {
                if (res) {
                    console.log(res)
                    setTimeout(() => {
                        EventRegister.emit('snackBar', res.message)
                    }, 500);
                }
                onSubmit()
            })
        }
    }

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Tell us why</Text>
                <View style={{ alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}></Text>
                </View>
                <View style={{ width: '100%' }}>
                    <View style={{ flexDirection: 'row', marginVertical: 2 }}>
                        <Text onPress={() => setIndex(0)} style={[styles.textInfoModalContent]}>
                            <Image source={index == 0 ?
                                require('../assets/icons/radio-check.png')
                                : require('../assets/icons/radio.png')
                            } style={[styles.iconMini]} />
                            {" "}Feels like spam</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginVertical: 2 }}>
                        <Text onPress={() => setIndex(1)} style={[styles.textInfoModalContent]}>
                            <Image source={index == 1 ?
                                require('../assets/icons/radio-check.png')
                                : require('../assets/icons/radio.png')
                            } style={[styles.iconMini]} />
                            {" "}Rude</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginVertical: 2 }}>
                        <Text onPress={() => setIndex(2)} style={[styles.textInfoModalContent]}>
                            <Image source={index == 2 ?
                                require('../assets/icons/radio-check.png')
                                : require('../assets/icons/radio.png')
                            } style={[styles.iconMini]} />
                            {" "}Inappropriate</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginVertical: 2 }}>
                        <Text onPress={() => setIndex(3)} style={[styles.textInfoModalContent]}>
                            <Image source={index == 3 ?
                                require('../assets/icons/radio-check.png')
                                : require('../assets/icons/radio.png')
                            } style={[styles.iconMini]} />
                            {" "}Others</Text>
                    </View>
                </View>
                {index == 3 ?
                    <TextInput
                        multiline={true}
                        maxLength={200}
                        onChangeText={setOther}
                        placeholder={"Enter other reason"}
                        textAlignVertical={'top'}
                        style={{
                            borderRadius: 4, borderColor: '#D1D1D1', marginTop: 10,
                            paddingHorizontal: 14, height: 100, borderWidth: 1, width: '100%'
                        }}
                    /> : null
                }
                <Button
                    title={"Submit"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={isValid} />
            </View>
        </ReactNativeModal>
    )
}


export function reportUser(isVisible, id, onBackButtonPress, onSubmit) {
    const [index, setIndex] = useState(0);
    const [other, setOther] = useState('');

    function isValid() {
        if (index == 3 && other.length == 0) {
            Toast.show("Other reason field cannot be empty.")
        } else {
            let reason
            if (index == 0) {
                reason = "Feels like spam"
            } else if (index == 1) {
                reason = "Rude"
            } else if (index == 2) {
                reason = "Inappropriate"
            } else if (index == 3) {
                reason = other
            }
            let body = {
                "user_report": id,
                "reason": reason
            }
            Api.call('report_user', 'POST', JSON.stringify(body)).then(res => {
                console.log(res)
                setTimeout(() => {
                    EventRegister.emit('snackBar', res.message)
                }, 500);
                onSubmit()
            })
        }
    }

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
                <View style={styles.containerModalInfo}>
                    <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                        <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                    </TouchableOpacity>
                    <Text style={styles.textInfoModalTitle}>Report this user?</Text>
                    <View style={{ marginBottom: 10, marginTop: 5, alignItems: 'center' }}>
                        <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Are you sure you want to report{'\n'}this user?</Text>
                    </View>

                    <View style={{ width: '100%' }}>
                        <View style={{ flexDirection: 'row', marginVertical: 2 }}>
                            <Text onPress={() => setIndex(0)} style={[styles.textInfoModalContent]}>
                                <Image source={index == 0 ?
                                    require('../assets/icons/radio-check.png')
                                    : require('../assets/icons/radio.png')
                                } style={[styles.iconMini]} />
                                {" "}Feels like spam</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginVertical: 2 }}>
                            <Text onPress={() => setIndex(1)} style={[styles.textInfoModalContent]}>
                                <Image source={index == 1 ?
                                    require('../assets/icons/radio-check.png')
                                    : require('../assets/icons/radio.png')
                                } style={[styles.iconMini]} />
                                {" "}Rude</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginVertical: 2 }}>
                            <Text onPress={() => setIndex(2)} style={[styles.textInfoModalContent]}>
                                <Image source={index == 2 ?
                                    require('../assets/icons/radio-check.png')
                                    : require('../assets/icons/radio.png')
                                } style={[styles.iconMini]} />
                                {" "}Inappropriate</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginVertical: 2 }}>
                            <Text onPress={() => setIndex(3)} style={[styles.textInfoModalContent]}>
                                <Image source={index == 3 ?
                                    require('../assets/icons/radio-check.png')
                                    : require('../assets/icons/radio.png')
                                } style={[styles.iconMini]} />
                                {" "}Others</Text>
                        </View>
                    </View>
                    {index == 3 ?
                        <TextInput
                            multiline={true}
                            maxLength={200}
                            onChangeText={setOther}
                            blurOnSubmit={true}
                            placeholder={"Enter other reason"}
                            textAlignVertical={'top'}
                            style={{
                                borderRadius: 4, borderColor: '#D1D1D1', marginTop: 10,
                                paddingHorizontal: 14, height: 100, borderWidth: 1, width: '100%'
                            }}
                        /> : null
                    }

                    <Button
                        title={"Submit"}
                        style={{ marginTop: 20, width: '100%' }}
                        backgroundColor={colors.appGold}
                        textColor={'white'}
                        onPress={isValid} />
                </View>
            </TouchableWithoutFeedback>
        </ReactNativeModal>
    )
}

export function cancelJob(isVisible, onBackButtonPress, onSubmit) {
    const [index, setIndex] = useState(0);
    const [reason, setReason] = useState("Feeling unwell");

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Cancel Job</Text>
                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Are you sure you want to cancel this job?</Text>
                </View>
                <View style={{ width: '100%' }}>

                    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                        <Text onPress={() => { setIndex(0); setReason('Feeling unwell') }} style={[styles.textInfoModalContent]}>
                            <Image source={index == 0 ?
                                require('../assets/icons/radio-check.png')
                                : require('../assets/icons/radio.png')
                            } style={[styles.iconMini]} />
                            {" "}Feeling unwell</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                        <Text onPress={() => { setIndex(1); setReason('Personal reasons') }} style={[styles.textInfoModalContent]}>
                            <Image source={index == 1 ?
                                require('../assets/icons/radio-check.png')
                                : require('../assets/icons/radio.png')
                            } style={[styles.iconMini]} />
                            {" "}Personal reasons</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                        <Text onPress={() => { setIndex(2); setReason('') }} style={[styles.textInfoModalContent]}>
                            <Image source={index == 2 ?
                                require('../assets/icons/radio-check.png')
                                : require('../assets/icons/radio.png')
                            } style={[styles.iconMini]} />
                            {" "}Others</Text>
                    </View>
                </View>

                {index == 2 ?
                    <TextInput
                        multiline={true}
                        maxLength={200}
                        value={reason}
                        onChangeText={setReason}
                        placeholder={"Enter other reason"}
                        textAlignVertical={'top'}
                        style={{
                            borderRadius: 4, borderColor: '#D1D1D1', marginTop: 10,
                            paddingHorizontal: 14, height: 100, borderWidth: 1, width: '100%'
                        }}
                    />
                    : null
                }

                <Button
                    title={"Submit"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={() => onSubmit(reason)} />
            </View>
        </ReactNativeModal>
    )
}

export function showJobLocations(isVisible, onBackButtonPress, locations) {
    const [items, setItems] = useState([]);
    const [temp, setTemp] = useState(0);

    useEffect(() => {
        let arr = []
        locations?.forEach(element => {
            arr.push({ name: element.location, selected: false, id: element.id })
        });
        setItems(arr)
    }, [locations])

    function selectItem(index) {
        let tempArr = items
        tempArr[index].selected = !tempArr[index].selected
        setItems(tempArr)
        setTemp(temp + 1)
    }

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Job Locations</Text>
                <View style={{ width: '100%', marginTop: 20 }}>
                    <FlatList
                        data={items}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity activeOpacity={1} onPress={() => selectItem(index)} style={{ flexDirection: 'row', paddingTop: 4, paddingBottom: 2, alignItems: 'center' }}>
                                    <Text style={[styles.textInfoModalContent]}>
                                        {index + 1}{". "}{item.name}</Text>
                                </TouchableOpacity>
                            )
                        }}
                    />

                </View>

            </View>
        </ReactNativeModal>
    )
}


export function applyJobLocation(isVisible, onBackButtonPress, locations, onSubmit) {
    const [items, setItems] = useState([]);
    const [temp, setTemp] = useState(0);

    useEffect(() => {
        let arr = []
        locations?.forEach(element => {
            arr.push({ name: element.location, selected: false, id: element.id })
        });
        setItems(arr)
    }, [locations])

    function selectItem(index) {
        let tempArr = items
        tempArr[index].selected = !tempArr[index].selected
        setItems(tempArr)
        setTemp(temp + 1)
    }

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Apply for Job</Text>
                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Are you sure you want to apply for{'\n'}this job?</Text>
                </View>
                <View style={{ width: '100%' }}>
                    <Text style={[styles.textInfoModalContent, { fontFamily: fonts.bold }]}>Location:</Text>
                    <FlatList
                        data={items}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity activeOpacity={1} onPress={() => selectItem(index)} style={{ flexDirection: 'row', paddingTop: 4, paddingBottom: 2, alignItems: 'center' }}>
                                    <Image source={item.selected ?
                                        require('../assets/icons/check-box-checked.png')
                                        : require('../assets/icons/check-box.png')
                                    } style={[styles.iconMini]} />
                                    <Text style={[styles.textInfoModalContent]}>
                                        {" "}{item.name}</Text>
                                </TouchableOpacity>
                            )
                        }}
                    />

                </View>

                <Button
                    title={"Apply"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appBlue}
                    textColor={'white'}
                    onPress={() => onSubmit(items)} />

                <Button
                    title={"Maybe Later"}
                    style={{ marginTop: 10, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={onBackButtonPress} />
            </View>
        </ReactNativeModal>
    )
}


export function applyAdhocJob(isVisible, onBackButtonPress, onSubmit) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Apply for Job</Text>
                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Are you sure you want to apply for{'\n'}this job?</Text>
                </View>

                <Button
                    title={"Apply"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appBlue}
                    textColor={'white'}
                    onPress={() => onSubmit()} />

                <Button
                    title={"Maybe Later"}
                    style={{ marginTop: 10, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={onBackButtonPress} />
            </View>
        </ReactNativeModal>
    )
}

export function appliedForJob(isVisible, onBackButtonPress, title, company) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Image source={require('../assets/icons/congratulation.png')} style={[styles.icon80, { marginTop: 10 }]} />

                <Text style={[styles.textInfoModalTitle, { color: '#21BD9D' }]}>Congratulations!</Text>
                <View style={{ alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>You have successfully applied as a{"\n"}
                            <Text style={[styles.textInfoModalContent, { fontFamily: fonts.bold }]}>{title}</Text>
                            <Text style={styles.textInfoModalContent}>{" "}at{" "}</Text>
                            <Text style={[styles.textInfoModalContent, { fontFamily: fonts.bold }]}>{company}.</Text>
                        </Text>
                    </View>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center', marginTop: 4 }]}>A confirmation of your application{'\n'}has been sent to your registered{'\n'}email. Good luck!</Text>
                </View>

            </View>
        </ReactNativeModal>
    )
}


export function submitFeedback(isVisible, onBackButtonPress, onSubmit) {
    const [feedback, setFeedback] = useState('');

    function onSubmitValid() {
        if (feedback.length == 0) {
            Toast.show("Feedback field cannot be empty.")
        } else {
            let body = {
                "feedback_message": feedback
            }
            Api.call('app_feedback', 'POST', JSON.stringify(body)).then(res => {
                onSubmit()
                setFeedback('')
                setTimeout(() => {
                    EventRegister.emit('snackBar', res.message)
                }, 500);
                console.log(res)
            })
        }
    }

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} >

                <View style={styles.containerModalInfo}>
                    <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                        <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                    </TouchableOpacity>
                    <Text style={styles.textInfoModalTitle}>Submit your feedback</Text>
                    <View style={{ marginVertical: 10, alignItems: 'center' }}>
                        <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>We would love to hear how we can{'\n'} improve. We shall respond within{'\n'}
                        2 working days. Thank you.</Text>
                    </View>

                    <TextInput
                        multiline={true}
                        maxLength={200}
                        value={feedback}
                        blurOnSubmit={true}
                        onChangeText={setFeedback}
                        placeholder={"Enter your feedback"}
                        textAlignVertical={'top'}
                        style={{
                            borderRadius: 4, borderColor: '#D1D1D1', marginTop: 10,
                            paddingHorizontal: 14, height: 100, borderWidth: 1, width: '100%'
                        }}
                    />

                    <Button
                        title={"Submit"}
                        style={{ marginTop: 20, width: '100%' }}
                        backgroundColor={colors.appGold}
                        textColor={'white'}
                        onPress={onSubmitValid} />
                </View>
            </TouchableWithoutFeedback>
        </ReactNativeModal>
    )
}

export function submitRatingForEmployer(isVisible, onBackButtonPress, onSubmit) {
    const [rating, setRating] = useState(0);
    const [review, setReview] = useState('');

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>

                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>

                <Text style={styles.textInfoModalTitle}>Rate your Employer!</Text>

                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Please submit your feedback to your{'\n'}employer.</Text>
                </View>

                <Text style={{ fontFamily: fonts.regular, fontSize: 10, color: 'gray' }}>Tap to rate</Text>

                <StarRating
                    disabled={false}
                    maxStars={5}
                    containerStyle={{ alignSelf: 'center', marginTop: 10, width: 140 }}
                    rating={rating}
                    selectedStar={value => setRating(value)}
                    starSize={24}
                    emptyStar={require('../assets/icons/star-yellow.png')}
                    fullStar={require('../assets/icons/star-yellow-fill.png')}
                />

                <TextInput
                    multiline={true}
                    maxLength={200}
                    value={review}
                    onChangeText={setReview}
                    placeholder={"Optional"}
                    textAlignVertical={'top'}
                    style={{
                        borderRadius: 4, borderColor: '#D1D1D1', marginTop: 20,
                        paddingHorizontal: 14, height: 60, borderWidth: 1, width: '100%'
                    }}
                />
                <View style={{ width: '100%', opacity: rating == 0 ? 0.6 : 1 }}>
                    <Button
                        title={"Submit"}
                        disabled={rating == 0}
                        style={{ marginTop: 10, width: '100%' }}
                        backgroundColor={colors.appGold}
                        textColor={'white'}
                        onPress={() => { onSubmit(rating, review); setRating(''); setReview('') }} />
                </View>
            </View>
        </ReactNativeModal>
    )
}

export function filterHourlyPrice(isVisible, onBackButtonPress, onSubmit) {
    const [price, setPrice] = useState(0);

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Enter your preferred hourly rate</Text>

                <View style={{
                    borderRadius: 4, borderColor: '#D1D1D1', marginTop: 10,
                    paddingHorizontal: 14, borderWidth: 1, width: '100%', height: 50, justifyContent: 'center'
                }}>
                    <TextInput
                        multiline={true}
                        maxLength={4}
                        placeholder={"Enter hourly rate"}
                        keyboardType={'number-pad'}
                        onChangeText={val => setPrice(val)}
                        style={{ bottom: 2 }}
                    />
                </View>
                <Button
                    title={"Submit"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={() => onSubmit(price)} />
            </View>
        </ReactNativeModal>
    )
}


export function passwordInfoModal(isVisible, onBackButtonPress) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Password guidelines</Text>
                <View style={{ width: '100%' }}>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignSelf: 'flex-start' }}>
                        <View style={styles.pointInModal}><Text>1</Text></View>
                        <Text style={styles.textInfoModalContent}>A minimum of 10 characters.</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 8, marginEnd: 20 }}>
                        <View style={styles.pointInModal}><Text>2</Text></View>

                        <View>
                            <Text style={[styles.textInfoModalContent, { marginBottom: 4 }]}>Include characters from at least three of the following types:</Text>
                            <View style={styles.rowCenter}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>Uppercase letters</Text>
                            </View>
                            <View style={styles.rowCenter}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>Lowercase letters</Text>
                            </View>
                            <View style={styles.rowCenter}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>Numbers</Text>
                            </View>
                            <View style={styles.rowCenter}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>Special Characters (e.g., $ ! @ #.)</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </ReactNativeModal>
    )
}


export function filterMonthlyPrice(isVisible, onBackButtonPress, onSubmit) {
    const [price, setPrice] = useState(0);
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Enter your preferred monthly salary</Text>
                <View style={{
                    borderRadius: 4, borderColor: '#D1D1D1', marginTop: 10,
                    paddingHorizontal: 14, borderWidth: 1, width: '100%', height: 50, justifyContent: 'center'
                }}>
                    <TextInput
                        multiline={true}
                        maxLength={4}
                        placeholder={"Enter monthly salary"}
                        keyboardType={'number-pad'}
                        onChangeText={val => setPrice(val)}
                        style={{ bottom: 2 }}
                    />
                </View>
                <Button
                    title={"Submit"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={() => onSubmit(price)} />
            </View>
        </ReactNativeModal>
    )
}


export function pointStructure(isVisible, onRewards, onShare, onBackButtonPress) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={[styles.containerModalInfo, { marginHorizontal: 0, alignItems: null, padding: 0 }]}>
                <ScrollView contentContainerStyle={{ padding: 20 }}>
                    <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                        <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                    </TouchableOpacity>
                    <Text style={styles.textInfoModalTitle}>Tier and Points Structure</Text>
                    <Text style={[{ alignSelf: 'flex-start', marginTop: 10, fontFamily: fonts.semiBold, fontSize: 16 }]}>Tiers</Text>
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <Image source={require('../assets/icons/points_line.png')}
                            style={{ width: 20, height: Platform.OS == 'android' ? 159 : 143, resizeMode: 'contain', marginTop: 10 }} />
                        <View style={{ flex: 1, height: 160 }}>
                            <View style={styles.pointRow}>
                                <Text style={[styles.textInfoModalContent]}>Member</Text>
                                <Text style={[styles.textInfoModalContent]}>Just joined</Text>
                            </View>
                            <View style={{ flex: 1 }} />
                            <View style={styles.pointRow}>
                                <View style={styles.rowCenter}>
                                    <Image source={require('../assets/icons/bronze.png')} style={[styles.iconMini, { marginEnd: 4 }]} />
                                    <Text style={[styles.textInfoModalContent]}>Bronze</Text>
                                </View>
                                <Text style={[styles.textInfoModalContent]}>1,000 pts</Text>
                            </View>
                            <View style={{ flex: 1 }} />
                            <View style={styles.pointRow}>
                                <View style={styles.rowCenter}>
                                    <Image source={require('../assets/icons/silver.png')} style={[styles.iconMini, { marginEnd: 4 }]} />
                                    <Text style={[styles.textInfoModalContent]}>Silver</Text>
                                </View>
                                <Text style={[styles.textInfoModalContent]}>2,500 pts</Text>
                            </View>
                            <View style={{ flex: 1 }} />
                            <View style={styles.pointRow}>
                                <View style={styles.rowCenter}>
                                    <Image source={require('../assets/icons/gold.png')} style={[styles.iconMini, { marginEnd: 4 }]} />
                                    <Text style={[styles.textInfoModalContent]}>Gold</Text>
                                </View>
                                <Text style={[styles.textInfoModalContent]}>7,500 pts</Text>
                            </View>
                            <View style={{ flex: 1 }} />
                            <View style={styles.pointRow}>
                                <View style={styles.rowCenter}>
                                    <Image source={require('../assets/icons/platinum.png')} style={[styles.iconMini, { marginEnd: 4 }]} />
                                    <Text style={[styles.textInfoModalContent]}>Platinum</Text>
                                </View>
                                <Text style={[styles.textInfoModalContent]}>15,000 pts</Text>
                            </View>
                            <View style={{ flex: 1 }} />
                            <View style={styles.pointRow}>
                                <View style={styles.rowCenter}>
                                    <Image source={require('../assets/icons/dimond.png')} style={[styles.iconMini, { marginEnd: 4 }]} />
                                    <Text style={[styles.textInfoModalContent]}>Diamond</Text>
                                </View>
                                <Text style={[styles.textInfoModalContent]}>30,000 pts</Text>
                            </View>
                        </View>
                    </View>
                    <Text style={[{ alignSelf: 'flex-start', marginTop: 10, fontFamily: fonts.semiBold, fontSize: 16 }]}>How to earn points</Text>
                    <View style={{ flexDirection: 'row', marginTop: 8, marginEnd: 20 }}>
                        <View style={styles.pointInModal}>
                            <Text>1</Text>
                        </View>
                        <View style={{ marginTop: 4 }}>
                            <Text style={[styles.textInfoModalContent, { marginBottom: 4 }]}>No. of hours worked.</Text>
                            <View style={styles.rowCenter}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>Half day shift = 50 pts</Text>
                            </View>
                            <Text style={{ fontFamily: fonts.regular, fontSize: 11, marginStart: 20 }}>(6 hours and less of work)</Text>
                            <View style={styles.rowCenter}>
                                <View style={styles.subpointBullet} />
                                <Text style={[styles.textInfoModalContent, { marginTop: 4 }]}>Full day shift = 100 pts</Text>
                            </View>
                            <Text style={{ fontFamily: fonts.regular, fontSize: 11, marginStart: 20 }}>(More than 6 hours of work)</Text>
                            <View style={{ flexDirection: 'row', marginTop: 4 }}>
                                <Text style={[styles.textInfoModalContent]}>Points can be used to redeem{" "}
                                    <Text onPress={onRewards} style={{ fontFamily: fonts.semiBold, color: colors.appBlue, textDecorationLine: 'underline' }}>rewards!</Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, marginEnd: 20, alignSelf: 'flex-start' }}>
                        <View style={styles.pointInModal}>
                            <Text>2</Text>
                        </View>
                        <View style={{ marginTop: 4 }}>
                            <Text style={[styles.textInfoModalContent, { marginBottom: 4 }]}>Rating System</Text>
                            <View style={[styles.rowCenter, { marginVertical: 2 }]}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>1 star (0.5x pts)</Text>
                            </View>
                            <View style={[styles.rowCenter, { marginVertical: 2 }]}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>2 star (1x pts)</Text>
                            </View>
                            <View style={[styles.rowCenter, { marginVertical: 2 }]}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>3 star (1.5x pts)</Text>
                            </View>
                            <View style={[styles.rowCenter, { marginVertical: 2 }]}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>4 star (2x pts)</Text>
                            </View>
                            <View style={[styles.rowCenter, { marginVertical: 2 }]}>
                                <View style={styles.subpointBullet} />
                                <Text style={styles.textInfoModalContent}>5 star (3x pts)</Text>
                            </View>
                            <Text style={[styles.textInfoModalContent, { marginTop: 4, lineHeight: 20 }]}>E.g. If you had worked a full day shift
                            {'\n'}(100 pts) and received a rating of 5 stars{'\n'}
    (3x pts) from your employer, you will{'\n'}receive a total of 300 points.</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, marginEnd: 20, alignSelf: 'flex-start' }}>
                        <View style={styles.pointInModal}>
                            <Text>3</Text>
                        </View>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 4 }}>

                            <Text style={[styles.textInfoModalContent, { lineHeight: 20 }]}>
                                <Text onPress={onShare} style={[{ fontFamily: fonts.semiBold, color: colors.appBlue, textDecorationLine: 'underline' }]}>Share</Text>
                                {" "}our app and earn up to 250{'\n'}points when your friends sign up{'\n'}using your unique link.
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        </ReactNativeModal>
    )
}


export function logout(isVisible, onBackButtonPress, onLogout) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Logout</Text>
                <View style={{ marginVertical: 5, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Are you sure you want to logout?</Text>
                </View>
                <Button
                    title={"Yes"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={onLogout} />
                <Button
                    title={"No"}
                    style={{ marginTop: 10, width: '100%' }}
                    backgroundColor={colors.appBlue}
                    textColor={'white'}
                    onPress={onBackButtonPress} />
            </View>
        </ReactNativeModal>
    )
}

export function exitFromApp(isVisible, onBackButtonPress, onExit) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>Exit</Text>
                <View style={{ marginVertical: 5, alignItems: 'center' }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Are you sure you want to exit the app?</Text>
                </View>
                <Button
                    title={"Yes"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={onExit} />
                <Button
                    title={"No"}
                    style={{ marginTop: 10, width: '100%' }}
                    backgroundColor={colors.appBlue}
                    textColor={'white'}
                    onPress={onBackButtonPress} />
            </View>
        </ReactNativeModal>
    )
}


export function mapModal(isVisible, onBackButtonPress, title, image) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                <Text style={styles.textInfoModalTitle}>{title}</Text>
                {title == 'North' ?
                    <Image source={require('../assets/icons/north.png')} style={{ height: 200, width: '100%', resizeMode: 'contain' }} />
                    : null}
                {title == 'North-East' ?
                    <Image source={require('../assets/icons/northest.png')} style={{ height: 200, width: '100%', resizeMode: 'contain' }} />
                    : null}
                {title == 'East' ?
                    <Image source={require('../assets/icons/east.png')} style={{ height: 200, width: '100%', resizeMode: 'contain' }} />
                    : null}
                {title == 'Central' ?
                    <Image source={require('../assets/icons/central.png')} style={{ height: 200, width: '100%', resizeMode: 'contain' }} />
                    : null}
                {title == 'West' ?
                    <Image source={require('../assets/icons/west.png')} style={{ height: 200, width: '100%', resizeMode: 'contain' }} />
                    : null}
            </View>
        </ReactNativeModal>
    )
}


export function showDateDetails(isVisible, onBackButtonPress, data, time, salary, location) {
    if (data.job_shifts_details)
        return (
            <ReactNativeModal isVisible={isVisible}
                backdropColor={colors.appBlue}
                onBackdropPress={onBackButtonPress}
                useNativeDriver={true}
                onBackButtonPress={onBackButtonPress}
                backdropOpacity={0.9}>
                <View style={[styles.containerModalInfo, { alignItems: 'flex-start' }]}>
                    <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                        <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                    </TouchableOpacity>
                    <Text style={[styles.textInfoModalTitle, { alignSelf: 'flex-start' }]}>{moment(data?.date_available).format("DD MMMM YYYY")}</Text>
                    <View style={{ marginVertical: 5, alignItems: 'flex-start' }}>

                        <View style={[styles.rowBetween, { alignItems: 'flex-start' }]}>
                            <Text style={[styles.fontSb]}>Minimum Commitment: </Text>
                            <Text style={[styles.textInfoModalContent, { flex: 1 }]}>{data?.job_shifts_details[0]?.min_no_of_days} days</Text>
                        </View>

                        <View style={styles.rowBetween}>
                            <Text style={[styles.fontSb]}>Time: </Text>
                            <Text style={[styles.textInfoModalContent]}>{
                                moment(data?.job_shifts_details[0]?.from_time).format('hh:mm a')
                                + " - " +
                                moment(data?.job_shifts_details[0]?.To_time).format('hh:mm a')
                            }</Text>
                        </View>

                        <View style={styles.rowBetween}>
                            <Text style={[styles.fontSb]}>Salary: </Text>
                            <Text style={[styles.textInfoModalContent]}>$&thinsp;{data?.job_shifts_details[0].salary}/{data?.job_shifts_details[0]?.salary_type}</Text>
                        </View>

                        <View style={[styles.rowBetween, { alignItems: 'flex-start' }]}>
                            <Text style={[styles.fontSb]}>Location: </Text>
                            <Text style={[styles.textInfoModalContent, { flex: 1 }]}>{data?.job_shifts_details[0]?.location}</Text>
                        </View>

                        <View style={styles.rowBetween}>
                            <Text style={[styles.fontSb]}>Slots Left: </Text>
                            <Text style={[styles.textInfoModalContent]}>{data?.job_shifts_details[0]?.slots_avaliable}</Text>
                        </View>
                    </View>
                </View>
            </ReactNativeModal>
        )
}

export function filterByJobType(isVisible, onBackButtonPress, apply) {

    // const [isPositive, setIsPositive] = useState(false);

    const [isAdHoc, setAdHoc] = useState(false);
    const [isPartTime, setPartTime] = useState(false);
    const [isFullTime, setFullTime] = useState(false);

    const [adHocModal, setAdHocModal] = useState(false);
    const [partTimeModal, setPartTimeModal] = useState(false);
    const [fullTimeModal, setFullTimeModal] = useState(false);

    const toggleAdhoc = () => {
        setAdHocModal(!adHocModal)
    }

    const togglePartTime = () => {
        setPartTimeModal(!partTimeModal)
    }

    const toggleFullTime = () => {
        setFullTimeModal(!fullTimeModal)
    }

    function submit() {

        let jobType = []
        if (isAdHoc) {
            jobType.push("1")
        } if (isPartTime) {
            jobType.push("2")
        } if (isFullTime) {
            jobType.push("3")
        }

        apply(jobType)
    }

    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>

                <View style={{ flexDirection: 'row', marginVertical: 20, marginHorizontal: 4, alignItems: 'center' }}>

                    <View style={{ width: '100%', alignItems: 'center' }}>

                        <View style={{ flexDirection: 'row', marginVertical: 16 }}>

                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => setAdHoc(!isAdHoc)}
                                    style={[styles.iconJobTypeModal, styles.darkShadow,
                                    { borderColor: isAdHoc ? colors.appBlue : '#DBDBDB' }]}>
                                    <Image source={require('../assets/icons/add-hoc.png')} style={[styles.icon32]} />
                                </TouchableOpacity>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
                                    <Text style={{ fontFamily: fonts.medium, marginEnd: 4 }}>Ad hoc</Text>
                                    <TouchableOpacity onPress={toggleAdhoc}>
                                        <Image source={require('../assets/icons/help.png')} style={[styles.iconMicro, { top: 2 }]} />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => setPartTime(!isPartTime)}
                                    style={[styles.iconJobTypeModal, styles.darkShadow,
                                    { borderColor: isPartTime ? colors.appBlue : '#DBDBDB' }]}>
                                    <Image source={require('../assets/icons/parttime.png')} style={[styles.icon32]} />
                                </TouchableOpacity>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
                                    <Text style={{ fontFamily: fonts.medium, marginEnd: 4 }}>Part Time</Text>
                                    <TouchableOpacity onPress={togglePartTime}>
                                        <Image source={require('../assets/icons/help.png')} style={[styles.iconMicro, { top: 2 }]} />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => setFullTime(!isFullTime)}
                                    style={[styles.iconJobTypeModal, styles.darkShadow,
                                    { borderColor: isFullTime ? colors.appBlue : '#DBDBDB' }]}>
                                    <Image source={require('../assets/icons/full-time.png')} style={[styles.icon32]} />
                                </TouchableOpacity>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
                                    <Text style={{ fontFamily: fonts.medium, marginEnd: 4 }}>Full Time</Text>
                                    <TouchableOpacity onPress={toggleFullTime}>
                                        <Image source={require('../assets/icons/help.png')} style={[styles.iconMicro, { top: 2 }]} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        <Button
                            title={"Apply"}
                            style={{ marginTop: 10, width: '100%' }}
                            backgroundColor={colors.appGold}
                            textColor={'white'}
                            onPress={submit} />
                    </View>
                    {adHoc(adHocModal, toggleAdhoc)}
                    {partTime(partTimeModal, togglePartTime)}
                    {fullTime(fullTimeModal, toggleFullTime)}
                </View>
            </View>
        </ReactNativeModal>
    )
}


export function filterByJobAvail(isVisible, onBackButtonPress, apply) {

    const [isAdHoc, setAdHoc] = useState(false);
    const [selectedDates, setSelectedDates] = useState([]);

    function selectDate(date) {
        if (selectedDates.length == 2)
            setSelectedDates([])
        setSelectedDates(arr => arr.concat(date.dateString))

        if (selectedDates.length == 1 && moment(date.dateString).isBefore(selectedDates[0])) {
            let arr = []
            arr.push(date.dateString)
            arr.push(selectedDates[0])
            setSelectedDates(arr)
        }
    }

    function removeDate(date) {
        setSelectedDates(arr => arr.filter(item => item != date.dateString))
    }


    function renderDateComponent(date, state) {
        let day = moment(date.dateString).format('ddd')
        if (state != 'disabled' && selectedDates.includes(date.dateString))
            return (
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

                    {selectedDates.length == 2 && day == 'Sun' ?
                        <View style={{
                            width: '50%', position: 'absolute', left: moment(date.dateString).isBefore(selectedDates[1]) ? '40%' : null,
                            height: 24, backgroundColor: '#E5E8ED', right: moment(date.dateString).isAfter(selectedDates[0]) ? '40%' : null,
                            borderTopStartRadius: 12, borderBottomStartRadius: 12
                        }} />
                        : day == 'Sat' ?
                            <View style={{
                                width: '50%', position: 'absolute', left: moment(date.dateString).isBefore(selectedDates[1]) ? '40%' : null,
                                height: 24, backgroundColor: '#E5E8ED', right: moment(date.dateString).isAfter(selectedDates[0]) ? '40%' : null,
                                borderTopEndRadius: 12, borderBottomEndRadius: 12
                            }} />
                            : selectedDates.length == 2 ? <View style={{
                                width: '50%', position: 'absolute', left: moment(date.dateString).isBefore(selectedDates[1]) ? '40%' : null,
                                height: 24, backgroundColor: '#E5E8ED', right: moment(date.dateString).isAfter(selectedDates[0]) ? '40%' : null,
                            }} /> : null
                    }

                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => { removeDate(date) }}
                        style={{
                            backgroundColor: colors.appBlue, marginVertical: 2,
                            justifyContent: 'center', height: 36, width: 36, borderRadius: 18
                        }}>
                        <Text style={[styles.calendarDate, { color: 'white' }]}>
                            {date.day}
                        </Text>
                    </TouchableOpacity>
                </View>
            );
        else if (state != 'disabled' && selectedDates.length == 2 && moment(date.dateString).isBefore(selectedDates[1]) && moment(date.dateString).isAfter(selectedDates[0]))
            return (
                <TouchableOpacity
                    style={{ height: 40, width: '100%', justifyContent: 'center' }}
                    onPress={() => selectDate(date)}>
                    <Text style={[
                        styles.calendarDate, { backgroundColor: '#E5E8ED' },
                        day == 'Sun' ? styles.sunBack : null,
                        day == 'Sat' ? styles.satBack : null]}>
                        {date.day}
                    </Text>
                </TouchableOpacity>
            );
        else if (state != 'disabled')
            return (
                <TouchableOpacity
                    style={{ height: 40, justifyContent: 'center' }}
                    onPress={() => selectDate(date)}>
                    <Text style={[styles.calendarDate, { color: 'black' }]}>
                        {date.day}
                    </Text>
                </TouchableOpacity>
            );
        else {
            return (
                <View style={{ height: 40, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[styles.calendarDate, { color: '#CACACA' }]}>
                        {date.day}
                    </Text>
                </View>
            );
        }
    }


    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            style={{ margin: 0 }}
            backdropOpacity={0.9}>

            <SafeAreaView style={{ backgroundColor: 'white', height: '100%', paddingTop: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text style={{ fontFamily: fonts.medium, marginStart: 16 }}>Select a date or a range</Text>
                    <TouchableOpacity onPress={onBackButtonPress} style={{ padding: 10 }}>
                        <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1 }}>
                    <CalendarList
                        hideExtraDays={false}
                        theme={{
                            textMonthFontFamily: fonts.semiBold,
                            textDayHeaderFontFamily: fonts.medium,
                            textDayFontFamily: fonts.medium,
                            arrowColor: 'black',
                            textSectionTitleColor: 'black',
                            textDayHeaderFontSize: 12,
                        }}
                        dayComponent={({ date, state }) => renderDateComponent(date, state)}
                        scrollEnabled={true}
                        markingType="custom"
                        pastScrollRange={2}
                        minDate={new Date()}
                        markedDates={[]}
                        renderHeader={(date) => {
                            return <View style={styles.calendarHeaderContainer}>
                                <Divider color={"gray"} style={{ flex: 1 }} />
                                <Text style={{ fontSize: 16, fontFamily: fonts.semiBold }}>{moment(new Date(date)).utc().format('MMM YYYY')}</Text>
                                <Divider color={"gray"} style={{ flex: 1 }} />
                            </View>
                        }}
                        renderArrow={() => <View style={{ width: '100%', flex: 1 }}><Divider color={"gray"} style={{ width: '100%' }} /></View>}
                    />
                </View>
                <Button
                    title={"Apply"}
                    style={{ margin: 10, marginBottom: 10 }}
                    backgroundColor={colors.appGold}
                    textStyle={{ fontSize: 16 }}
                    textColor={'white'}
                    onPress={() => apply(selectedDates)} />
            </SafeAreaView>
        </ReactNativeModal>
    )
}


export function filterBySalary(isVisible, onBackButtonPress, apply) {

    const [hourlyRate, setHourlyRate] = useState([15]);
    const [monthlySalary, setMonthlySalary] = useState([4500]);
    const [priceModal, setPriceModal] = useState(false);
    const [monthlyPriceModal, setMonthlyPriceModal] = useState(false);

    const [hourlySelected, sethourlySelected] = useState(false);
    const [monthlySelected, setmonthlySelected] = useState(false);

    return (

        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            // style={{ margin: 0 }}
            backdropOpacity={0.9}>


            <View style={{ backgroundColor: 'white', borderRadius: 10 }}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>

                <View style={[styles.rowCenter, { margin: 20, marginBottom: 0 }]}>
                    <Text style={{ fontSize: 12, fontFamily: fonts.medium }}>Select preferred{" "}</Text>
                    <Text style={{ fontSize: 12, fontFamily: fonts.bold, color: colors.appBlue }}>hourly rate</Text>
                </View>
                <TouchableOpacity activeOpacity={1} onPress={() => { sethourlySelected(!hourlySelected); setmonthlySelected(false) }}
                    style={{ borderRadius: 4, borderColor: hourlySelected ? colors.appBlue : '#CCD3DC', borderWidth: 1, margin: 20, padding: 10, paddingVertical: 20, alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, fontFamily: fonts.bold }}>Above ${hourlyRate}</Text>
                    {/* <Text style={{ fontSize: 12, fontFamily: fonts.regular, color: 'gray' }}>Tap to manually enter preferred rate</Text> */}

                    <MultiSlider
                        customMarker={() => <Text style={{ height: 30 }}><Image source={require('../assets/icons/above-arrow.png')} style={styles.iconSmall} /></Text>}
                        trackStyle={{ height: 4, alignSelf: 'center' }}
                        sliderLength={250}
                        onValuesChange={(value) => { setHourlyRate(value); sethourlySelected(true); setmonthlySelected(false) }}
                        markerOffsetY={Platform.OS == 'ios' ? 10 : 2}
                        selectedStyle={{
                            backgroundColor: '#e3e3e3',
                        }}
                        max={50}
                        values={hourlyRate}
                        containerStyle={{ height: 20, marginTop: 20 }}
                        unselectedStyle={{
                            backgroundColor: '#e3e3e3',
                        }}
                    />

                    <View style={[styles.rowBetween, { width: '100%' }]}>
                        <Text style={{ fontSize: 12, fontFamily: fonts.regular }}>$0</Text>
                        <Text style={{ fontSize: 12, fontFamily: fonts.regular }}>$50</Text>
                    </View>

                </TouchableOpacity>

                <View style={[styles.rowCenter, { margin: 20, marginBottom: 0 }]}>
                    <Text style={{ fontSize: 12, fontFamily: fonts.medium }}>Select preferred{" "}</Text>
                    <Text style={{ fontSize: 12, fontFamily: fonts.bold, color: colors.appBlue }}>monthly salary</Text>
                </View>

                <TouchableOpacity activeOpacity={1} onPress={() => { setmonthlySelected(!monthlySelected); sethourlySelected(false) }} style={{ borderRadius: 4, borderColor: monthlySelected ? colors.appBlue : '#CCD3DC', borderWidth: 1, margin: 20, padding: 10, paddingVertical: 20, alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, fontFamily: fonts.bold }}>Above ${monthlySalary}</Text>
                    {/* <Text style={{ fontSize: 12, fontFamily: fonts.regular, color: 'gray' }}>Tap to manually enter preferred salary</Text> */}

                    <MultiSlider
                        customMarker={() => <Text style={{ height: 30 }}><Image source={require('../assets/icons/above-arrow.png')} style={styles.iconSmall} /></Text>}
                        trackStyle={{ height: 4 }}
                        sliderLength={250}
                        markerOffsetY={Platform.OS == 'ios' ? 10 : 2}
                        onValuesChange={(value) => {
                            setMonthlySalary(value);
                            setmonthlySelected(true)
                            sethourlySelected(false)
                        }}
                        selectedStyle={{
                            backgroundColor: '#e3e3e3',
                        }}
                        values={monthlySalary}
                        max={5000}
                        step={500}
                        containerStyle={{ height: 20, marginTop: 20 }}
                        unselectedStyle={{
                            backgroundColor: '#e3e3e3',
                        }}
                    />

                    <View style={[styles.rowBetween, { width: '100%' }]}>
                        <Text style={{ fontSize: 12, fontFamily: fonts.regular }}>$0</Text>
                        <Text style={{ fontSize: 12, fontFamily: fonts.regular }}>$5000</Text>
                    </View>
                </TouchableOpacity>

                <Button
                    title={"Apply"}
                    style={{ margin: 10, marginBottom: 10 }}
                    backgroundColor={colors.appGold}
                    textStyle={{ fontSize: 16 }}
                    textColor={'white'}
                    onPress={() => apply({ monthly: monthlySalary, hourly: hourlyRate }, hourlySelected, monthlySelected)} />
            </View>
        </ReactNativeModal>
    )
}


export function showCoupon(isVisible, onBackButtonPress, company, amount, onCopy) {
    return (
        <ReactNativeModal isVisible={isVisible}
            backdropColor={colors.appBlue}
            onBackdropPress={onBackButtonPress}
            useNativeDriver={true}
            onBackButtonPress={onBackButtonPress}
            backdropOpacity={0.9}>
            <View style={styles.containerModalInfo}>
                <TouchableOpacity onPress={onBackButtonPress} style={{ position: 'absolute', padding: 10, right: 0 }}>
                    <Image source={require('../assets/icons/cross.png')} style={[styles.iconMini]} />
                </TouchableOpacity>
                {/* <Text style={styles.textInfoModalTitle}>Coupon Code</Text> */}
                <View style={{ marginVertical: 5, flexDirection:'row',flexWrap:'wrap'  }}>
                    <Text style={[styles.textInfoModalContent, { textAlign: 'center' }]}>Thank you for redeeming the ${amount} {company} voucher! We will send the voucher to your registered email shortly.</Text>
                </View>
                <Button
                    title={"Ok"}
                    style={{ marginTop: 20, width: '100%' }}
                    backgroundColor={colors.appGold}
                    textColor={'white'}
                    onPress={onBackButtonPress} />
            </View>
        </ReactNativeModal>
    )
}