import LoaderManager from "./LoaderManager"
import SimpleToast from "react-native-simple-toast"

export default Notifier = {
    send: function (token, title, body) {
        let notiBody = {
            "to": token,
            "notification": {
                "title": title,
                "body": body,
            },
        }
        console.log(notiBody)
        return fetch(`https://fcm.googleapis.com/fcm/send`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": "key=AAAAgqaehDg:APA91bGgxxPWbKWOWgR9WQh8yPfR6WNLR06Ghgr9k1GtqMDPE1nbWmQTrb9qDyTZ_BYhDLxJQmzSFQQ8-I_krIF86in8zz4Q3oH_LUc-OG2tWVgjWVY8vL-_Zwvyc1zmfozah-O_jCrg"
            },
            body: JSON.stringify(notiBody)
        }).then(res => {
            console.log(res)
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    }
}