import dynamicLinks from '@react-native-firebase/dynamic-links';
import Share from "react-native-share";
import Const from './Const';

export default LinkShare = {
    share: async function(jobId, jobName) {
        // const link =
        //     new firebase.links.DynamicLink(global.url + restaurantId, 'https://gigorg.page.link')
        //         .android.setPackageName('com.gig')
        //         .ios.setBundleId('com.gig');

        const link = await dynamicLinks().buildLink({
            link: Const.shareUrl + 'job/' + jobId,
            // domainUriPrefix is created in your Firebase console
            domainUriPrefix: 'https://gigorg.page.link',
            ios: {
                bundleId: 'com.gigapp',
                appStoreId: '1555620746'
            },
            android: {
                packageName: 'com.gigsolutions'
            }
        });

        console.log(link)

        // firebase.links()
        //     .createDynamicLink(link)
        //     .then((url) => {
        //         console.log("hiii", url)
        const shareOptions = {
            title: jobName,
            message: 'Checkout the Gig job!',
            url: link
        };
        Share.open(shareOptions)
            .then((res) => { console.log(res); })
            .catch((err) => { err && console.log(err); });
        // });
    }
}