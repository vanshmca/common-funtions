import Api from "./Api"

export default Methods = {
    saveJob: async function (jobId, isSaved) {
        let body = {
            "job_id": jobId,
            "saved_status": isSaved ? "0" : "1"
        }
        return Api.call('save_jobs', 'POST', JSON.stringify(body)).then(res => {
            if (res.status == 1) {
                return true
            }
            return false
        })
    }
}