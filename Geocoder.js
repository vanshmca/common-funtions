import LoaderManager from "./LoaderManager"
import SimpleToast from "react-native-simple-toast"

export default Geocoder = {
    get: function (lat, long, apiKey) {
        LoaderManager.show()
        return fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${long}&key=${apiKey}`, {
            method: 'GET',
        }).then(res => {
            LoaderManager.hide()
            console.log(res)
            return res.json()
        }).then(resJson => {
            if (resJson.error_message) {
                SimpleToast.show(resJson.error_message)
            } else {
                return resJson
            }
        }).catch(err => {
            LoaderManager.hide()
            console.log(err)
        })
    }
}