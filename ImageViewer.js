import React, { useRef, useState, useEffect } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { View, Image, TouchableOpacity } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import colors from './colors';

export function ImageViewer(isVisible, imageUrl, onBackButtonPress) {

    return <ReactNativeModal isVisible={isVisible}
        useNativeDriver={true}
        onBackButtonPress={onBackButtonPress}
        backdropColor={colors.appBlue}
        backdropOpacity={0.9}>
        <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={onBackButtonPress}>
                <Image style={{ height: 20, width: 20, marginVertical: 10, marginStart: 10, resizeMode: 'contain' }} source={require('../assets/icons/cross-white.png')} />
            </TouchableOpacity>
            <Image source={{ uri: imageUrl }} style={{ width: '100%', height: '100%', resizeMode: 'contain', flex: 1 }} />
        </View>
    </ReactNativeModal>
}