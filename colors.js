export default colors = {
    appBlue: '#022152',
    appGold: '#d2bf94',
    appBack: '#F6F8FA',
    appText: '#000000'
}