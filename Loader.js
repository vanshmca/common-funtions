import React, { Component } from 'react';
import { ActivityIndicator, View, Dimensions, Modal, Platform } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import colors from './colors';



class Loader extends Component {

    render() {
        return (
            <View>
                <ReactNativeModal
                    isVisible={this.props.visible}
                    animationIn='fadeIn'
                    animationOut='fadeOut'
                    backdropColor={colors.appBlue}
                    useNativeDriver={true}
                >
                    <ActivityIndicator
                        size='large'
                        color={colors.appGold}
                    />
                </ReactNativeModal>
            </View>
        )
    }
}

export default Loader