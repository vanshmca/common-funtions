"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _LoaderManager = _interopRequireDefault(require("./LoaderManager"));

var _reactNativeSimpleToast = _interopRequireDefault(require("react-native-simple-toast"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = Notifier = {
  send: function send(token, title, body) {
    var notiBody = {
      "to": token,
      "notification": {
        "title": title,
        "body": body
      }
    };
    console.log(notiBody);
    return fetch("https://fcm.googleapis.com/fcm/send", {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Authorization": "key=AAAAgqaehDg:APA91bGgxxPWbKWOWgR9WQh8yPfR6WNLR06Ghgr9k1GtqMDPE1nbWmQTrb9qDyTZ_BYhDLxJQmzSFQQ8-I_krIF86in8zz4Q3oH_LUc-OG2tWVgjWVY8vL-_Zwvyc1zmfozah-O_jCrg"
      },
      body: JSON.stringify(notiBody)
    }).then(function (res) {
      console.log(res);
      return true;
    })["catch"](function (err) {
      console.log(err);
      return false;
    });
  }
};

exports["default"] = _default;