"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _firestore = _interopRequireDefault(require("@react-native-firebase/firestore"));

var _database = _interopRequireDefault(require("@react-native-firebase/database"));

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var employeesRef = (0, _database["default"])().ref('/employees');
var usersCollection = (0, _firestore["default"])().collection('employees');

var _default = ChatHelper = {
  sendMessage: function sendMessage(chatRoomId, userId, data, user, employerDetails) {
    return (0, _database["default"])().ref("/employees/".concat(userId, "/myChatrooms/").concat(chatRoomId, "/messages")).push().set(data).then(function () {
      return (0, _database["default"])().ref("/employees/".concat(userId, "/myChatrooms/").concat(chatRoomId)).update({
        lastMessage: data,
        chatRoomId: chatRoomId,
        employeeDetails: {
          name: user.name,
          image: user.image,
          id: userId
        },
        employerDetails: employerDetails
      }).then(function () {
        return (0, _database["default"])().ref("/employeer/".concat(employerDetails.id, "/myChatrooms/").concat(chatRoomId)).update({
          lastMessage: data,
          chatRoomId: chatRoomId,
          employeeDetails: {
            name: user.name,
            image: user.image,
            id: userId
          },
          employerDetails: employerDetails
        }).then(function () {
          return (0, _database["default"])().ref("/employeer/".concat(employerDetails.id, "/myChatrooms/").concat(chatRoomId, "/messages")).push().set(data).then(function () {
            (0, _database["default"])().ref("/employeer/".concat(employerDetails.id, "/myChatrooms/").concat(chatRoomId, "/unreadCount")).transaction(function (unreadCount) {
              return unreadCount = unreadCount + 1;
            });
            return true;
          });
        });
      });
    });
  },
  clearUnread: function clearUnread(userId, chatRoomId) {
    return (0, _database["default"])().ref("/employees/".concat(userId, "/myChatrooms/").concat(chatRoomId, "/unreadCount")).transaction(function (unreadCount) {
      return unreadCount = 0;
    });
  },
  setLastSeen: function setLastSeen(userId) {
    return (0, _database["default"])().ref("/employees/".concat(userId)).update({
      lastSeen: (0, _moment["default"])().format()
    }).then(function (res) {
      console.log(res);
      return true;
    })["catch"](function (err) {
      console.log(err);
      return false;
    });
  },
  setOnline: function setOnline(userId, isOnline) {
    return (0, _database["default"])().ref("/employees/".concat(userId)).update({
      isOnline: isOnline
    }).then(function (res) {
      console.log(res);
      return true;
    })["catch"](function (err) {
      console.log(err);
      return false;
    });
  },
  statusListener: function statusListener(employerId) {
    return (0, _database["default"])().ref("/employees/".concat(employerId));
  },
  listenerOnChatroom: function listenerOnChatroom(userId, chatRoomId) {
    return (0, _database["default"])().ref("/employees/".concat(userId, "/myChatrooms/").concat(chatRoomId, "/messages"));
  },
  listenerOnRoom: function listenerOnRoom() {
    return (0, _database["default"])().ref("/rooms");
  },
  listenerOnMyChatrooms: function listenerOnMyChatrooms(id) {
    return (0, _database["default"])().ref("/employees/".concat(id, "/myChatrooms"));
  },
  deleteChat: function deleteChat(userId, chatRoomId) {
    return (0, _database["default"])().ref("/employees/".concat(userId, "/myChatrooms/").concat(chatRoomId)).remove().then(function (value) {
      console.log(value);
      return true;
    })["catch"](function (err) {
      console.log(err);
      return false;
    });
  },
  checkChatRoom: function checkChatRoom(id) {
    return (0, _database["default"])().ref("/chatrooms/".concat(id)).once('value').then(function (val) {
      if (val.exists()) {
        return true;
      } else {
        return false;
      }
    });
  },
  createChatRoom: function createChatRoom(id, data) {
    return (0, _database["default"])().ref("/chatrooms/".concat(id)).set({
      employeeName: data.name
    }).then(function (val) {
      return true;
    })["catch"](function (err) {
      console.log(err);
      return false;
    });
  },
  addUser: function addUser(data) {
    return (0, _database["default"])().ref("/employees/".concat(data.id)).set({
      id: data.id,
      image: data.image,
      name: data.name,
      email: data.email,
      myChatrooms: []
    }).then(function (res) {
      console.log(res);
    })["catch"](function (err) {
      return console.log(err);
    });
  },
  checkUser: function checkUser(id) {
    return (0, _database["default"])().ref("/employees/".concat(id)).once('value').then(function (doc) {
      if (doc.exists()) {
        return true;
      } else {
        return false;
      }
    })["catch"](function (err) {
      return console.log(err);
    });
  }
};

exports["default"] = _default;