
import { StyleSheet, Dimensions, Platform } from 'react-native';
import colors from './colors';

export const fonts = {
    bold: 'HKGrotesk-Bold',
    regular: 'HKGrotesk-Regular',
    semiBold: 'HKGrotesk-SemiBold',
    medium: 'HKGrotesk-Medium',
}

export default styles = StyleSheet.create({
    //Containers
    container: {
        flex: 1
    },
    absCenter: {
        position: 'absolute', height: '100%', width: '100%',
        justifyContent: 'center', alignItems: 'center'
    },
    containerBlue: {
        flex: 1,
        backgroundColor: colors.appBlue,
        width: '100%',
    },
    containerApp: {
        backgroundColor: colors.appBack, flex: 1,
        borderTopStartRadius: 20, borderTopEndRadius: 20
    },
    containerCenter: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerSocial: {
        height: 60, width: 60,
        backgroundColor: 'white', borderRadius: 8, justifyContent: 'center',
        margin: 12, marginBottom: 0, alignItems: 'center'
    },
    // containerSocial: {
    //     height: 50, width: 50, borderWidth: 1, borderColor: '#DCDCDC',
    //     backgroundColor: 'white', borderRadius: 8,
    //     margin: 12, marginBottom: 0, alignItems: 'center'
    // },
    containerContent: {
        borderTopEndRadius: 20, alignItems: 'center', paddingHorizontal: 8, paddingVertical: 20,
        borderTopStartRadius: 20, backgroundColor: 'white', flex: 1, height: '100%'
    },
    containerScrollContent: {
        borderTopEndRadius: 20, paddingHorizontal: 8, paddingVertical: 20,
        borderTopStartRadius: 20, backgroundColor: 'white',
        flexGrow: 1, alignItems: 'center'
    },
    containerScrollContentBox: {
        paddingHorizontal: 8, paddingVertical: 20,
        backgroundColor: 'white',
        flexGrow: 1, alignItems: 'center'
    },
    containerModalInfo: {
        backgroundColor: 'white', alignItems: 'center',
        borderRadius: 8, marginHorizontal: 20, padding: 20
    },

    containerBanner: { width: '100%', flex: 1, resizeMode: 'contain' },
    banner: { width: '100%', flex: 1 },

    //Shadow
    shadow: {
        elevation: 2,
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
    },
    darkShadow: {
        elevation: 4,
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
    },
    extraDarkShadow: {
        elevation: 8,
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
    },
    //Icon Sizes
    iconMicro: {
        height: 12,
        width: 12,
        resizeMode: 'contain'
    },
    iconMini: {
        height: 16,
        width: 16,
        resizeMode: 'contain'
    },
    iconSmall: {
        height: 20,
        width: 20,
        resizeMode: 'contain'
    },
    iconDefault: {
        height: 24,
        width: 24,
        resizeMode: 'contain'
    },
    iconLarge: {
        height: 40,
        width: 40,
        resizeMode: 'contain'
    },
    iconSocial: {
        height: 60,
        width: 60,
        resizeMode: 'contain'
    },
    imageUser: {
        height: 80,
        width: 80
    },
    icon80: {
        resizeMode: 'contain',
        height: 80,
        width: 80
    },
    icon50: {
        height: 50, width: 50,
        resizeMode: 'contain',
    },
    icon100: {
        resizeMode: 'contain',
        height: 100,
        width: 100
    },
    iconJobType: {
        resizeMode: 'contain',
        height: 80,
        borderRadius: 40,
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'center',
        width: 80,
        borderColor: colors.appBlue,
        borderWidth: 1,
    },
    iconJobTypeModal: {
        resizeMode: 'contain',
        height: 60,
        borderRadius: 30,
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'center',
        width: 60,
        borderColor: colors.appBlue,
        borderWidth: 1,
    },
    icon60: {
        height: 60,
        borderRadius: 30,
        width: 60
    },
    icon32: {
        height: 32,
        width: 32,
        resizeMode: 'contain'
    },
    //Colors 
    colorWhite: {
        color: 'white'
    },
    ///padding
    pad16: {
        padding: 16
    },
    p10: {
        padding: 10
    },
    ph16: {
        paddingHorizontal: 16
    },
    ph10: {
        paddingHorizontal: 10
    },
    pv8: {
        paddingVertical: 8
    },
    pv12: {
        paddingVertical: 12
    },
    pv10: {
        paddingVertical: 10
    },
    pv20: {
        paddingVertical: 20
    },
    pad8: {
        padding: 8
    },
    pad4: {
        padding: 4
    },
    //Margin
    mh12: {
        marginHorizontal: 12
    },
    //Fonts
    fontSb: {
        fontFamily: fonts.semiBold
    },
    fontR: {
        fontFamily: fonts.regular
    },
    //Styles
    horCenter: {
        flexDirection: 'row', alignItems: 'center'
    },
    rowCenter: {
        flexDirection: 'row', alignItems: 'center'
    },
    center: {
        justifyContent: 'center', alignItems: 'center'
    },
    /////////////////////////
    textSocialLogin: {
        fontSize: 12, color: 'white',
        margin: 8, textAlign: 'center', fontFamily: fonts.medium
    },
    contentTitle: {
        fontSize: 28, marginVertical: 5,
        textAlign: 'center', fontFamily: fonts.bold
    },
    contentSubtitle: {
        textAlign: 'center', lineHeight: 18,
        marginBottom: 20, color: '#767676',
        fontSize: 12, fontFamily: fonts.medium
    },
    snackBarContainer: {
        position: 'absolute', justifyContent: 'center', borderTopStartRadius: 20, borderTopEndRadius: 20,
        bottom: 0, backgroundColor: '#21BD9D', height: 50, width: '100%'
    },
    snackBarText: { color: 'white', paddingHorizontal: 20, fontSize: 16 },
    textInfoModalTitle: { fontSize: 18, alignSelf: 'center', fontFamily: fonts.bold, marginTop: 5 },
    textInfoModalContent: { fontFamily: fonts.regular },
    topLogo: { height: 60, alignSelf: 'center', resizeMode: 'contain', marginBottom: 40 },
    chipUnselected: {
        fontFamily: fonts.medium, borderColor: '#dbdbdb',
        borderWidth: 1, borderRadius: 2, padding: 2, paddingHorizontal: 8,
        margin: 4,
    },
    chipSelected: {
        fontFamily: fonts.medium,
        borderWidth: 1, borderRadius: 2, padding: 2, paddingHorizontal: 8,
        margin: 4, color: 'white', backgroundColor: colors.appBlue
    },
    badgeStyle: {
        position: 'absolute', height: 16, width: 16,
        borderRadius: 8, right: 0, backgroundColor: '#FFAB2C',
        justifyContent: 'center', alignItems: 'center'
    },
    badgeTextStyle: { fontFamily: fonts.bold, fontSize: 12, alignSelf: 'center', top: Platform.OS == 'ios' ? 0 : -1, color: 'white' },
    rowBetween: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    jobCardContainer: {
        backgroundColor: 'white', marginHorizontal: 16,
        marginVertical: 8, borderRadius: 2, padding: 10
    },
    jobCardTitle: { fontFamily: fonts.semiBold, fontSize: 16 },
    jobCardCompany: { fontSize: 12, color: colors.appGold, fontFamily: fonts.medium },
    jobCardSaveJob: { fontSize: 12, color: colors.appBlue, fontFamily: fonts.medium },
    textSb12: { fontSize: 12, fontFamily: fonts.semiBold },
    textR12: { fontSize: 12, fontFamily: fonts.regular },
    reg10: { fontSize: 10, fontFamily: fonts.regular },

    notiContainer: { flexDirection: 'row', flexWrap: 'wrap', paddingBottom: 8, padding: 16 },
    notiCompName: { fontFamily: fonts.semiBold, color: colors.appBlue },
    notiPost: { fontFamily: fonts.semiBold },
    notiText: { fontFamily: fonts.regular },
    notiTimeAgo: { fontFamily: fonts.regular, fontSize: 12, color: 'gray', marginStart: 16, marginBottom: 16 },

    //Chat
    containerMyMsg: {
        paddingVertical: 10, backgroundColor: colors.appBlue,
        justifyContent: 'center',
        paddingHorizontal: 20, borderTopStartRadius: 20, borderTopEndRadius: 20,
        borderBottomStartRadius: 20
    },
    textMyMsg: {
        color: 'white', fontFamily: fonts.medium
    },
    containerOtherMsg: {
        paddingVertical: 10, backgroundColor: 'white',
        justifyContent: 'center',
        paddingHorizontal: 20, borderTopStartRadius: 20, borderTopEndRadius: 20,
        borderBottomEndRadius: 20
    },
    textOtherMsg: {
        color: 'black', fontFamily: fonts.medium
    },
    textMyName: { alignSelf: 'flex-end', marginEnd: 20, fontFamily: fonts.semiBold, marginBottom: 4, fontSize: 12 },
    textOtherName: { marginStart: 20, fontFamily: fonts.semiBold, marginBottom: 4, fontSize: 12 },
    textMyDate: { fontSize: 10, color: '#AAAAAA', marginStart: 16, marginTop: 4 },
    textOtherDate: { fontSize: 10, color: '#AAAAAA', marginEnd: 16, marginTop: 4, alignSelf: 'flex-end' },

    ///Discover
    discoverFilterContainer: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
    discoverFilterText: { fontFamily: fonts.semiBold, fontSize: 12, marginHorizontal: 4, bottom: Platform.OS == 'ios' ? 0 : 1 },
    discoverFilterMainContainer: { flexDirection: 'row', marginHorizontal: 16, marginBottom: 8, backgroundColor: 'white', flex: 1, paddingHorizontal: 8 },

    ///Profile
    profileTitle: { fontFamily: fonts.semiBold, marginTop: 10 },
    profileValue: { fontFamily: fonts.regular },
    profileHyperlink: {
        fontFamily: fonts.medium, color: colors.appBlue,
        height: 20, marginBottom: 10, marginStart: 10, textDecorationLine: 'underline'
    },
    pointRow: { flexDirection: 'row', justifyContent: 'space-between', width: '100%', marginVertical: 5 },
    subpointBullet: {
        height: 5, width: 5, alignSelf: 'center',
        borderRadius: 2, backgroundColor: colors.appBlue,
        top: 2, marginEnd: 8
    },
    pointInModal: {
        borderRadius: 12, borderColor: '#e5e5e5', marginEnd: 8,
        borderWidth: 1, height: 24, width: 24, alignItems: 'center', justifyContent: 'center'
    },
    pointContainer: {
        borderColor: '#385077', borderWidth: 1, borderRadius: 4,
        padding: 8, marginVertical: 8, flexDirection: 'row', alignItems: 'center'
    },
    textEarnMore: {
        color: colors.appBlue, fontSize: 12, color: 'white',
        textDecorationLine: 'underline', fontFamily: fonts.medium
    },
    profileTopContainer: {
        width: '100%', backgroundColor: colors.appBlue, justifyContent: 'center',
        paddingHorizontal: 20, flexDirection: 'row', alignItems: 'center'
    },
    inviteContainer: {
        backgroundColor: 'white', borderRadius: 4, marginStart: 4, marginVertical: 8,
        alignItems: 'center', paddingHorizontal: 8, paddingVertical: 12, height: 60,
        justifyContent: 'center'
    },

    //Job Details
    jobDetailCard: { margin: 15, marginVertical: 7.5, padding: 10, backgroundColor: 'white' },
    jobDetailButton: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', },
    jobDetailButtonText: { fontFamily: fonts.semiBold, fontSize: 12, color: 'white', marginStart: 8 },
    jobDetailHighItem: { fontFamily: fonts.medium, fontSize: 12, marginStart: 6, marginVertical: 4 },
    jobDetailItem: { fontFamily: fonts.medium, fontSize: 12, marginVertical: 4 },
    seeMore: {
        fontFamily: fonts.bold, color: colors.appBlue, fontSize: 14,
        textDecorationLine: 'underline', paddingTop: 4
    },
    jobDetailDescHeading: { fontFamily: fonts.bold, fontSize: 16 },
    addMore: {
        color: colors.appBlue, fontSize: 12, marginHorizontal: 4, alignSelf: 'flex-end',
        textDecorationLine: 'underline', fontFamily: fonts.medium
    },
    dateContainer: { height: 40, width: 50, alignItems: 'center', justifyContent: 'center' },
    simpleDateText: {
        textAlign: 'center', fontSize: 16, fontFamily: fonts.medium,
    },
    availableDateContainer: { backgroundColor: '#E5E8ED', justifyContent: 'center', height: 35, width: 35, borderRadius: 17 },
    selectedDateContainer: { backgroundColor: colors.appBlue, justifyContent: 'center', height: 35, width: 35, borderRadius: 17 },
    infoIconCont: { position: 'absolute', top: -5, right: 0, paddingStart: 4, paddingBottom: 4, alignSelf: 'flex-start' },


    //Setting
    settingItemText: { fontFamily: fonts.bold, fontSize: 16, marginHorizontal: 10, flex: 1 },
    settingItem: { flexDirection: 'row', alignItems: 'center', padding: 16 },

    //Filters
    filterTab: {
        backgroundColor: '#E3E3E3', paddingHorizontal: 20, alignItems: 'center',
        justifyContent: 'center', height: 60
    },
    filterTabActive: {
        backgroundColor: 'white', paddingHorizontal: 20, alignItems: 'center',
        justifyContent: 'center', height: 60
    },
    filterTabText: { fontFamily: fonts.medium, color: '#949494', fontSize: 12 },
    filterTabTextActive: { fontFamily: fonts.medium, fontSize: 12, color: colors.appBlue },
    calendarDate: {
        textAlign: 'center', fontSize: 16, fontFamily: fonts.medium, height: 24,
    },
    sunBack: {
        borderTopLeftRadius: 12,
        borderBottomLeftRadius: 12
    },
    satBack: {
        borderTopRightRadius: 12,
        borderBottomRightRadius: 12
    },
    calendarHeaderContainer: {
        height: 50, alignSelf: 'center', alignItems: 'center',
        justifyContent: 'center', flexDirection: 'row', width: Dimensions.get('screen').width,
        flex: 1
    },
    selectedBack: {
        height: 24, backgroundColor: '#E5E8ED', width: '50%', position: 'absolute',
    },
    textRadio: { fontFamily: fonts.medium, marginStart: 4, bottom: Platform.OS == 'ios' ? 0 : 1 },
    textIndustry: { fontFamily: fonts.medium, fontSize: 12, lineHeight: 15, marginTop: 4, textAlign: 'center', height: 50 },
    containerIndustry: { alignItems: 'center', width: 80, flex: 1 },
    contactUsImage: { height: 140, marginBottom: 20, resizeMode: 'contain', alignSelf: 'center' },
    contactUsItemImage: { width: 50, height: 50, resizeMode: 'contain', alignSelf: 'center' },
    contactUsItemLabel: { fontFamily: fonts.semiBold, fontSize: 10, color: '#909090' },
    contactUsItemValue: { fontFamily: fonts.medium, fontSize: 12, },

    //Rewards
    couponContainer: {
        borderRadius: 4, flex: 1, marginHorizontal: 10,
        marginVertical: 5
    },
    containerImage: { flex: 1, width: '100%', height: 140, overflow: 'hidden', borderRadius: 4 },
    greenPriceContainer: {
        height: 100, width: 100,
        borderRadius: 50, backgroundColor: '#11B44C', padding: 10,
        position: 'absolute', right: -30, top: -30, justifyContent: 'center',
        alignItems: 'center'
    },
    couponPrice: { fontFamily: fonts.bold, fontSize: 20, marginTop: 20, marginRight: 20, color: 'white' },

    bigInput: { height: Platform.OS == 'android' ? null : 90, marginVertical: Platform.OS == 'android' ? 0 : 6 },
    noDataContainer: { position: 'absolute', height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' },
    fontMono: { fontFamily: Platform.OS == 'android' ? 'monospace' : null },
})