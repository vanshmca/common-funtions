import React, { useRef, useState, useEffect } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { View, Image, TouchableOpacity, Keyboard, SafeAreaView } from 'react-native';
import ReactNativeModal from 'react-native-modal';
import colors from './colors';
import Const from './Const';

export function AutoCompleteModal(isVisible, onBackButtonPress, onSubmit) {
    const inputRef = useRef()

    // useEffect(() => {
    //     setTimeout(() => {
    //         inputRef.current.focus()
    //     }, 500);
    // }, [])

    return <ReactNativeModal isVisible={isVisible}
        useNativeDriver={true}
        onBackButtonPress={onBackButtonPress}
        backdropColor={colors.appBlue}
        backdropOpacity={0.9}>
        <SafeAreaView style={{ flex: 1, flexDirection: 'row' }}>
            <GooglePlacesAutocomplete
                placeholder='Search'
                styles={{
                    row: { backgroundColor: 'white' }
                }}
                focusable={true}
                autoFocus={true}
                enablePoweredByContainer={false}
                debounce={1000}
                showSoftInputOnFocus={true}
                style={{ width: '100%', flex: 1, backdropColor: 'white' }}
                onPress={(data, details = null) => {
                    console.log(details);
                    onSubmit(details)
                }}
                query={{
                    key: Const.googleSecretKey,
                    language: 'en',
                }}
            />

            <TouchableOpacity onPress={onBackButtonPress}>
                <Image style={{ height: 20, width: 20, marginVertical: 10, marginStart: 10, resizeMode: 'contain' }} source={require('../assets/icons/cross-white.png')} />
            </TouchableOpacity>
        </SafeAreaView>
    </ReactNativeModal>
}