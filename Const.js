export default Const = {

    baseUrl: "http://54.179.224.99:8080/",
    shareUrl: "http://gigsolutions.sg/",
    imageUrl: "http://54.179.224.99:8080",
    googleSecretKey: "AIzaSyAyiC-K1_Bxfbh6jCaG3zs_Y_5PxrsXUGE",

    industries: [
        [
            { icon: require('../assets/icons/retail.png'), title: 'Retail', isSelected: false },
            { icon: require('../assets/icons/food-breverages.png'), title: 'F & B', isSelected: false }
        ],
        [
            { icon: require('../assets/icons/bank.png'), title: 'Banking & Finance', isSelected: false },
            { icon: require('../assets/icons/logastics.png'), title: 'Logistics & Supply Chain', isSelected: false }
        ],
        [
            { icon: require('../assets/icons/fairs-event.png'), title: 'Fairs & Events', isSelected: false },
            { icon: require('../assets/icons/hospitaltiy.png'), title: 'Hospitality & Tourism', isSelected: false }
        ],
        [
            { icon: require('../assets/icons/hr.png'), title: 'HR', isSelected: false },
            { icon: require('../assets/icons/fitness.png'), title: 'Fitness & Sports', isSelected: false }
        ],
        [
            { icon: require('../assets/icons/driver.png'), title: 'Drivers & Delivery', isSelected: false },
            { icon: require('../assets/icons/childhood.png'), title: 'Early Childhood', isSelected: false }
        ],
        [
            { icon: require('../assets/icons/marketing.png'), title: 'Marketing & PR', isSelected: false },
            { icon: require('../assets/icons/sec.png'), title: 'Security', isSelected: false }
        ],
        [
            { icon: require('../assets/icons/it.png'), title: 'IT', isSelected: false },
            { icon: require('../assets/icons/accounting.png'), title: 'Accounting', isSelected: false },
        ],
        [
            { icon: require('../assets/icons/beautiful.png'), title: 'Beauty & Wellness', isSelected: false },
        ],
    ],

    departments: [
        [
            { icon: require('../assets/icons/admin.png'), title: 'Admin', isSelected: false },
            { icon: require('../assets/icons/customer-service.png'), title: 'Customer Service', isSelected: false }
        ],
        [
            { icon: require('../assets/icons/opration.png'), title: 'Operations', isSelected: false },
            { icon: require('../assets/icons/persional-assistant.png'), title: 'Personal Assistant', isSelected: false }
        ],
        [
            { icon: require('../assets/icons/telemarkting.png'), title: 'Telemarketing', isSelected: false },
            { icon: require('../assets/icons/other.png'), title: 'Others', isSelected: false }
        ],
    ]
}